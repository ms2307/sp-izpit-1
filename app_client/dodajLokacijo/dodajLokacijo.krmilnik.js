(function() {
  function dodajLokacijoCtrl($routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija) {
    var vm = this;
    
    vm.lokacija = {}
    vm.testniPodatki = function() {
      vm.lokacija.naziv = "Fakulteta za računalništvo in informatiko";
      vm.lokacija.naslov = "Večna pot 113, 1000 Ljubljana, Slovenija";
      vm.lokacija.lat = 46.050109;
      vm.lokacija.lng = 14.468922;
      vm.lokacija.lastnosti = "narava,znanje,računalništvo";
      vm.lokacija.dnevi1 = "ponedeljek - petek";
      vm.lokacija.odprtje1 = "06:00";
      vm.lokacija.zaprtje1 = "21:00";
      vm.lokacija.dnevi2 = "vikend";
      vm.lokacija.zaprto2 = true;
    };

    vm.validateInput = function() {
      if (!vm.lokacija.naziv || !vm.lokacija.naslov || !vm.lokacija.lat || !vm.lokacija.lng || !vm.lokacija.dnevi1 || !vm.lokacija.dnevi2) {
        vm.napaka()
        return false
      }

      if (!vm.lokacija.zaprto1 && (!vm.lokacija.odprtje1 || !vm.lokacija.zaprtje1)) {
        vm.napaka('Popravite 1. termin delovnega časa!')
        return false
      }

      if (!vm.lokacija.zaprto2 && (!vm.lokacija.odprtje2 || !vm.lokacija.zaprtje2)) {
        vm.napaka('Popravite 2. termin delovnega časa!')
        return false
      }

      return true
    }

    vm.napaka = function (str) {
      vm.napakaNaObrazcu = str || 'Zahtevani so vsi podatki, prosim poskusite znova!';
    }

    vm.posiljanjePodatkov = function () {
      vm.napakaNaObrazcu = ''
      if (!avtentikacija.jePrijavljen()) {
        vm.napakaNaObrazcu = 'Za dodajanje lokacij se morate prijaviti'
        return
      }
      if (vm.validateInput()) {
        // POST /lokacije
        edugeocachePodatki.dodajanjeLokacije(vm.lokacija)
          .then(
            function success() {
              location.href = '/'
            }, 
            function error() {
              vm.napakaNaObrazcu = 'Prislo je do napake na strezniku'
            }
          )
      }
    }

  }
  dodajLokacijoCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('edugeocache')
    .controller('dodajLokacijoCtrl', dodajLokacijoCtrl);
})();