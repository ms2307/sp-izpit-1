(function() {
  var edugeocachePodatki = function($http, avtentikacija) {
    var koordinateTrenutneLokacije = function(lat, lng) {
      return $http.get(
        'api/lokacije?lng=' + lng /* '14.469027' */ + 
          '&lat=' + lat /* '46.050129' */ + 
          '&maxRazdalja=100');
    };
    var podrobnostiLokacijeZaId = function(idLokacije) {
      return $http.get('/api/lokacije/' + idLokacije);
    };
    var dodajanjeLokacije = function(podatki) {
      return $http.post('/lokacije', podatki);
    };
    var dodajKomentarZaId = function(idLokacije, podatki) {
      return $http.post('/api/lokacije/' + idLokacije + '/komentarji', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    return {
      koordinateTrenutneLokacije: koordinateTrenutneLokacije,
      podrobnostiLokacijeZaId: podrobnostiLokacijeZaId,
      dodajanjeLokacije: dodajanjeLokacije,
      dodajKomentarZaId: dodajKomentarZaId
    };
  };
  edugeocachePodatki.$inject = ['$http', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('edugeocache')
    .service('edugeocachePodatki', edugeocachePodatki);
})();