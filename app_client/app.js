(function() {
  function nastavitev($routeProvider, $locationProvider, $sceDelegateProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'seznam/seznam.pogled.html',
        controller: 'seznamCtrl',
        controllerAs: 'vm'
      })
      .when('/informacije', {
        templateUrl: 'skupno/pogledi/genericnoBesedilo.pogled.html',
        controller: 'informacijeCtrl',
        controllerAs: 'vm'
      })
      .when('/lokacija/:idLokacije', {
        templateUrl: '/podrobnostiLokacije/podrobnostiLokacije.pogled.html',
        controller: 'podrobnostiLokacijeCtrl',
        controllerAs: 'vm'
      })
      .when('/dodajLokacijo', {
        templateUrl: '/dodajLokacijo/dodajLokacijo.pogled.html',
        controller: 'dodajLokacijoCtrl',
        controllerAs: 'vm'
      })
      .when('/registracija', {
        templateUrl: '/avtentikacija/registracija/registracija.pogled.html',
        controller: 'registracijaCtrl',
        controllerAs: 'vm'
      })
      .when('/prijava', {
        templateUrl: '/avtentikacija/prijava/prijava.pogled.html',
        controller: 'prijavaCtrl',
        controllerAs: 'vm'
      })
      .otherwise({redirectTo: '/'});
    $locationProvider.html5Mode(true);

    $sceDelegateProvider.resourceUrlWhitelist([
      'self', 'https://maps.google.com/maps?q=**'
    ]);
  }
  
  /* global angular */
  angular
    .module('edugeocache', ['ngRoute', 'ngSanitize', 'ui.bootstrap'])
    .config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', nastavitev]);
})();